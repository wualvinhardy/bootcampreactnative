//1) Array to Object
console.log("1) Array to Object");
function arrayToObject(arr) {
    var peopleObject = {};
    var now = new Date();
    var thisYear = now.getFullYear();

    for(var i=0; i<arr.length; i++){
        var birthyear = arr[i][3];
        var ageMessage;
        if(birthyear > thisYear || birthyear == undefined){
            ageMessage = "Invalid Birth Year";
        }
        else{
            ageMessage = thisYear - arr[i][3];
        }

        var peopleData = {
            firstname: arr[i][0],
            lastname: arr[i][1],
            gender: arr[i][2],
            age: ageMessage
        }
        var fullName = (i+1) + ". " + arr[i][0] + " " + arr[i][1];
        peopleObject[fullName] = peopleData;
    }
    
    console.log(peopleObject); 
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
{
  '1. Bruce Banner': { 
    firstName: 'Bruce', 
    lastName: 'Banner', 
    gender: 'male', 
    age: 45 
  },
  '2. Natasha Romanoff': {
    firstName: 'Natasha',
    lastName: 'Romanoff',
    gender: 'female',
    age: 'Invalid Birth Year'
  }
} 
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/* 
{
  '1. Tony Stark': {
    firstName: 'Tony', 
    lastName: 'Stark', 
    gender: 'male', 
    age: 40 
  },
  '2. Pepper Pots': {
    firstName: 'Pepper',
    lastName: 'Pots',
    gender: 'female',
    age: 'Invalid Birth Year'
  }
} 
*/
 
// Error case 
arrayToObject([]) // "{}"

//2) Shopping Time
console.log("\n2) Shopping Time");
function shoppingTime(memberId, money) {
    // you can only write your code here!
    const discountedProduct = {
        'Sepatu Stacattu' :  1500000,
        'Baju Zoro' : 500000,
        'Baju H&N' : 250000,
        'Sweater Uniklooh' : 175000,
        'Casing Handphone' : 50000
    }

    if(memberId == undefined || memberId == ''){
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }
    else if(money == undefined || money < 50000){
        return "Mohon maaf, uang tidak cukup";
    }
    else{
        let result = {};
        let arrPurchased = [];
        let changeMoney = money;
        for(const product in discountedProduct){
            if(changeMoney >= discountedProduct[product]){
                changeMoney -= discountedProduct[product];
                arrPurchased.push(product);
            }
        }

        result['memberId'] = memberId;
        result['money'] = money;
        result['listPurchased'] = arrPurchased;
        result['changeMoney'] = changeMoney;
        return result;
    }
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

//3) Naik Angkot
console.log("\n3) Naik Angkot");
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    const basicFare = 2000;
    let result = [];
    for(let i=0; i<arrPenumpang.length; i++){
        let tripData = {}
        tripData['penumpang'] = arrPenumpang[i][0];
        tripData['naikDari'] = arrPenumpang[i][1];
        tripData['tujuan'] = arrPenumpang[i][2];
        let passedRoute = Math.abs(rute.indexOf(tripData['naikDari']) - rute.indexOf(tripData['tujuan']));
        tripData['bayar'] = passedRoute * basicFare;
        result.push(tripData)
    }
    return result;
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]