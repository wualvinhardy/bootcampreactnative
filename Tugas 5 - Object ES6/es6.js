//1) Mengubah fungsi menjadi fungsi arrow
console.log("1) Mengubah fungsi menjadi fungsi arrow");
const golden = () => {
    console.log("this is golden!!")
}

golden()

//2) Sederhanakan menjadi Shorthand Object literal di ES6
console.log("\n2) Sederhanakan menjadi Shorthand Object literal di ES6");
const newFunction = (firstName, lastName) => {
    const fullName = () =>{
        console.log(firstName + " " + lastName)
        return;
    }
    
    return {
        firstName,
        lastName,
        fullName
    }
}
   
//Driver Code
const person = newFunction("William", "Imoh");
console.log(person);
person.fullName();  

/*Output
{
    firstName: 'William',
    lastName: 'Imoh',
    fullName: [Function: fullName]
}
William Imoh 
*/

//3) Destructuring
console.log("\n3) Destructuring");
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation, spell} = newObject;

// Driver code
console.log(firstName, lastName, destination, occupation)

//4) Array Spreading
console.log("\n4) Array Spreading");
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)

//5) String Template Literals
console.log("\n5) String Template Literals");
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}  do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

 
// Driver Code
console.log(before) 