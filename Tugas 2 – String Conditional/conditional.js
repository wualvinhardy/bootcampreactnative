//1) If Else
console.log("1)")
var nama = "Alvin"
var peran = "GuarD"

if (nama == ""){
    console.log("Nama harus diisi!")
}
else if (peran == ""){
    console.log("Halo "+ nama + ", pilih peranmu untuk memulai game!")
}
else{
    peran = peran[0].toUpperCase() + peran.slice(1).toLowerCase();
    var welcomelog1 = "Selamat datang di Dunia Werewolf, " + nama;
    var welcomelog2 = "Halo " + peran + " " + nama;

    if (peran == "Penyihir"){
        welcomelog2 = welcomelog2 + ", kamu dapat melihat siapa yang menjadi werewolf!"
    }
    else if (peran == "Guard"){
        welcomelog2 = welcomelog2 + ", kamu akan membantu melindungi temanmu dari serangan werewolf."
    }
    else if (peran == "Werewolf"){
        welcomelog2 = welcomelog2 + ", kamu akan memakan mangsa setiap malam!"
    }

    console.log(welcomelog1);
    console.log(welcomelog2);
}

//2) Switch Case
console.log("\n2)")
var tanggal = 29; 
var bulan = 2; 
var tahun = 2024;
var mindate;
var maxdate;

switch(bulan){
    case 1:{
        bulan = "Januari";
        mindate = 1;
        maxdate = 31;
        break;
    }
    case 2:{
        bulan = "Februari";
        mindate = 1;

        if(tahun % 4 != 0){
            maxdate = 28;
        }
        else if(tahun % 100 != 0){
            maxdate = 29;
        }
        else if(tahun % 400 != 0){
            maxdate = 28;
        }
        else{
            maxdate = 29;
        }
        break;
    }
    case 3:{
        bulan = "Maret";
        mindate = 1;
        maxdate = 31;
        break;
    }
    case 4:{
        bulan = "April";
        mindate = 1;
        maxdate = 30;
        break;
    }
    case 5:{
        bulan = "Mei";
        mindate = 1;
        maxdate = 31;
        break;
    }
    case 6:{
        bulan = "Juni";
        mindate = 1;
        maxdate = 30;
        break;
    }
    case 7:{
        bulan = "Juli";
        mindate = 1;
        maxdate = 30;
        break;
    }
    case 8:{
        bulan = "Agustus";
        mindate = 1;
        maxdate = 31;
        break;
    }
    case 9:{
        bulan = "September";
        mindate = 1;
        maxdate = 30;
        break;
    }
    case 10:{
        bulan = "Oktober";
        mindate = 1;
        maxdate = 29;
        break;
    }
    case 11:{
        bulan = "November";
        mindate = 1;
        maxdate = 30;
        break;
    }
    case 12:{
        bulan = "Desember";
        mindate = 1;
        maxdate = 31;
        break;
    }
    default:{
        console.log("Bulan tidak valid!")
    }
}

if(typeof bulan === 'string'){
    if(tanggal < mindate || tanggal > maxdate){
        console.log("Tanggal tidak valid!")
    }
    else{
        console.log(tanggal + " " + bulan + " " + tahun);
    }
}
