//1) Animal Class
console.log("1) Animal Class");
class Animal {
    // Code class di sini
    constructor(name, legs = 4, cold_blooded = false){
        this.a_name = name;
        this.a_legs = legs;
        this.a_cold_blooded = cold_blooded;
    }

    get name(){
        return this.a_name;
    }

    get legs(){
        return this.a_legs;
    }

    get cold_blooded(){
        return this.a_cold_blooded;
    }
}
 
const sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// Code class Ape dan class Frog di sini  
class Ape extends Animal {
    constructor(name, legs = 2, cold_blooded = false){
        super(name, legs, cold_blooded);
    }

    yell(){
        console.log("Auooo");
    }
}

class Frog extends Animal{
    constructor(name, legs = 4, cold_blooded = true){
        super(name, legs, cold_blooded);
    }

    jump(){
        console.log("hop hop");
    }
}

const sungokong = new Ape("kera sakti");
console.log(sungokong.name); // "kera sakti"
console.log(sungokong.legs); // 2
console.log(sungokong.cold_blooded); // false
sungokong.yell(); // "Auooo"

const kodok = new Frog("buduk");
console.log(kodok.name); // "buduk"
console.log(kodok.legs); // 4
console.log(kodok.cold_blooded); // true
kodok.jump(); // "hop hop"

//2) Function to Class
console.log("\n2) Function to Class");

//Class dalam bentuk function
/*
function Clock({ template }) {
    var timer;

    function render() {
        var date = new Date();
  
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
  
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
  
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
  
        var output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
        console.log(output);
    }
  
    this.stop = function() {
        clearInterval(timer);
    };
  
    this.start = function() {
        render();
        timer = setInterval(render, 1000);
    };  
}
*/

class Clock {
    // Code di sini
    constructor({template}){
        this.template = template;
        this.timer;
    }

    render(){
        var date = new Date();

        var hours = date.getHours();
        if(hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

        console.log(output);
    }

    stop(){
        clearInterval(this.timer);
    }

    start(){
        this.render();
        this.timer = setInterval(this.render.bind(this), 1000);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 