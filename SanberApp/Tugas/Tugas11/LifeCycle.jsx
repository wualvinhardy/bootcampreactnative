import React, { useEffect, useState } from 'react';
import { Button, View, Text } from 'react-native';

const LifeCycle = () => {
  // Deklarasi variabel state baru yang kita sebut "count"
  const [countIncrement, setCountIncrement] = useState(0);
  const [countDecrement, setCountDecrement] = useState(3000);
  useEffect(()=>{
    const timerRef = setInterval(() => {
      setCountIncrement(prevCount => prevCount + 1);
    },1000);
    
    return () => {
      clearInterval(timerRef)
    }
  }, []);
  useEffect(()=>{
    const timerRef = setInterval(() => {
      setCountDecrement(prevCount => prevCount - 1);
    },1000);
    
    return () => {
      clearInterval(timerRef)
    }
  }, []);

  return (
    <View style={{alignItems: 'center', justifyContent:"center", flex:1, paddingHorizontal:16}}>
      <Text>{countIncrement}</Text>
      <Text>{countDecrement}</Text>        
    </View>
  );
}

export default LifeCycle