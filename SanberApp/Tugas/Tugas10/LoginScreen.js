import React from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native'

export default function LoginScreen() {
    return (
        <View style={styles.container}>
            <Text style={styles.header}>
                Login
            </Text>
            <Image
                style={{height: 363, width: 308, resizeMode: 'contain', alignSelf: 'center'}}
                source={require('./assets/Innovation.png')}
            />
            <View style={[styles.inputField, {marginTop: 15, marginHorizontal: 75}]}>
                <Image
                    style={styles.avatarDrawwer}
                    source={require('./assets/user-128.png')}
                />
                <Text style={{color: '#F1C40F'}}>username</Text>
            </View>
            <View style={[styles.inputField, {marginTop: 15, marginHorizontal: 75}]}>
                <Image
                    style={styles.avatarDrawwer}
                    source={require('./assets/padlock-128.png')}
                />
                <Text style={{color: '#F1C40F'}}>password</Text>
            </View>
            <TouchableOpacity style={[styles.primaryButton, {marginTop: 30, marginHorizontal: 75}]}>
                <Text style={{color: 'white', textAlign: 'center'}}>Login</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        backgroundColor: '#50555C',
        flexDirection: 'column',
    },
    avatarDrawwer:{
        height:18,
        width:18,
        margin: 5
    },
    header:{
        //fontFamily: 'Inter',
        fontStyle: 'normal',
        fontWeight: "700",
        fontSize: 28,
        lineHeight: 34,
        textAlign: 'center',
        color: '#F1C40F',
        margin: 50
    },
    primaryButton:{
        borderRadius: 30,
        height: 50,
        backgroundColor:'#F1C40F',
        justifyContent: 'center'
    },
    inputField:{
        borderBottomColor:'#F1C40F',
        borderBottomWidth: 1,
        flexDirection: 'row'
    }
})

/*
<Image
                style={{height: 303, width: 258}}
                source={require('./assets/Innovation _Monochromatic.png')}
            />
*/