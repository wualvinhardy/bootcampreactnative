import React from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native'

export default function AboutScreen() {
    return (
        <View style={styles.container}>
            <Text style={[styles.header, {textAlign:'left'}]}>
                About Me
            </Text>
            <View style={{flexDirection: 'row'}}>
                <Image
                    style={[styles.aboutMe, {marginHorizontal: 30}]}
                    source={require('./assets/Me.jpg')}
                />
                <View style={{flexDirection: 'column', justifyContent:'center'}}>
                    <Text style={{
                        color: '#F1C40F',
                        fontSize: 24}}>
                        Wu Alvin Hardy
                    </Text>
                    <Text style={{
                        color: '#F1C40F',
                        fontSize: 16}}>
                        Developer
                    </Text>
                </View>
            </View>
            <View style={[styles.lineHeader, {marginHorizontal: 40, marginVertical: 20}]}>
                <Text style={{
                    color: '#F1C40F',
                    fontSize: 24,
                    textAlign: 'center'}}>
                    Portfolio
                </Text>
            </View>
            <View style={{flexDirection: 'row', marginHorizontal: 40}}>
                <Image
                    style={[styles.avatarDrawwer, {marginLeft: 40}]}
                    source={require('./assets/gitlab.png')}
                />
                <Text style={{
                    color: '#F1C40F',
                    fontSize: 20,
                    textAlign: 'center',
                    marginLeft: 40}}>
                    @wualvinhardy
                </Text>
            </View>
            <View style={[styles.lineHeader, {marginHorizontal: 40, marginTop: 60, marginBottom: 20}]}>
                <Text style={{
                    color: '#F1C40F',
                    fontSize: 24,
                    textAlign: 'center'}}>
                    My Contact
                </Text>
            </View>
            <View style={{flexDirection: 'row', marginHorizontal: 40}}>
                <Image
                    style={[styles.avatarDrawwer, {marginLeft: 40}]}
                    source={require('./assets/whatsapp.png')}
                />
                <Text style={{
                    color: '#F1C40F',
                    fontSize: 16,
                    textAlign: 'center',
                    marginLeft: 40}}>
                    08195564069
                </Text>
            </View>
            <View style={{flexDirection: 'row', marginHorizontal: 40}}>
                <Image
                    style={[styles.avatarDrawwer, {marginLeft: 40}]}
                    source={require('./assets/linkedin.png')}
                />
                <Text style={{
                    color: '#F1C40F',
                    fontSize: 16,
                    textAlign: 'center',
                    marginLeft: 40}}>
                    Wu Alvin Hardy
                </Text>
            </View>
            <View style={{flexDirection: 'row', marginHorizontal: 40}}>
                <Image
                    style={[styles.avatarDrawwer, {marginLeft: 40}]}
                    source={require('./assets/mail.png')}
                />
                <Text style={{
                    color: '#F1C40F',
                    fontSize: 16,
                    textAlign: 'center',
                    marginLeft: 40}}>
                    wualvinhardy@gmail.com
                </Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        backgroundColor: '#50555C',
        flexDirection: 'column',
    },
    aboutMe: {
        height: 116,
        width: 116,
        resizeMode: 'contain',
        borderRadius: 360,
    },
    header:{
        //fontFamily: 'Inter',
        fontStyle: 'normal',
        fontWeight: "700",
        fontSize: 28,
        lineHeight: 34,
        textAlign: 'center',
        color: '#F1C40F',
        margin: 50
    },
    lineHeader:{
        borderColor:'#F1C40F',
        borderTopWidth: 1,
        borderBottomWidth: 1
        
    },
    avatarDrawwer:{
        height:36,
        width:36,
        margin: 5
    },
})

/*
<Image
                style={{height: 303, width: 258}}
                source={require('./assets/Innovation _Monochromatic.png')}
            />
*/