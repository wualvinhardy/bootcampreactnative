// Copy Paste ke App.js untuk mencoba
import React from 'react';
import { Text, View } from 'react-native';

const HelloWorldApp = () => {
	const name = "Marus";
  return (
    <View style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
      }}>
      <Text numberOfLines={2}>Hello, world! {name}</Text>
    </View>
  );
}

export default HelloWorldApp;