//1) replaceCharacter
console.log("\n1) replaceCharacter");
function replaceCharacter(sentence, charReplaced, charReplacement=""){
    let result = ""
    for(i=0; i<sentence.length;i++){
        if(sentence[i] == charReplaced){
            result += charReplacement;
        }
        else{
            result += sentence[i];
        }
    }

    return result;
}

// Test Case 1
var sentence = "Pada Hari Minggu ku turut ayah ke kota";

var result = replaceCharacter(sentence, "a", "o");
console.log(result); //Podo Hori Minggu ku turut oyoh ke koto

// Test Case 2
var sentence = "Naik delman istimewa ku duduk di muka";

var result = replaceCharacter(sentence, "a");
console.log(result); //Nik delmn istimew ku duduk di muk

//2) sortGrade
console.log("\n2) sortGrade");

function sortGrade(studentData){
    studentData.sort((a, b) => {
        return b[2] - a[2] || a[0] - b[0];
    });
    return studentData;
}

// Format Data [absen, nama, nilai]
var studentData = [
	[2, "John Duro", 60],
	[4, "Robin Ackerman", 100],
	[1, "Jaeger Marimo", 60],
	[6, "Zoro", 80],
	[5, "Zenitsu", 80],
	[3, "Patrick Zala", 90],
];

var sortedData = sortGrade(studentData);
console.log(sortedData);
// Output
// [
// 	[4, "Robin Ackerman", 100],
// 	[3, "Patrick Zala", 90],
// 	[5, "Zenitsu", 80],
// 	[6, "Zoro", 80],
// 	[1, "Jaeger Marimo", 60],
// 	[2, "John Duro", 60],
// ];