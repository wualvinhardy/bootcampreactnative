const readBooksPromise = require('./promise.js')
 
const books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
var i = 0;
function startReadingBooks(timeLeft, listOfBooks){
    if(i < books.length && timeLeft > 0){
        readBooksPromise(timeLeft, listOfBooks[i])
        .then((timeLeft) => {
            i++;
            startReadingBooks(timeLeft, listOfBooks)
        })
        .catch((timeLeft) => {});
    }
}

var time = 10000;
startReadingBooks(time, books);