// di index.js
const readBooks = require('./callback.js')
 
const books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

// Tulis code untuk memanggil function readBooks di sini
var time = 10000;
var i = 0;
readBooks(time, books[i], function reading(timeLeft){
    if(timeLeft > 0 && i < books.length-1){
        i++;
        readBooks(timeLeft, books[i], reading);
    }
});