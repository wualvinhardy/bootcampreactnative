//1) Range
console.log("1) Range");
function range(startNum, finishNum) {
    var result = [];

    if(finishNum == undefined){
        result.push(-1);
    }
    else{
        do{
            result.push(startNum);
            if(startNum < finishNum){startNum++}
            else if(startNum > finishNum){startNum--}
        }while(startNum != finishNum);

        if(!result.includes(finishNum)){
            result.push(finishNum);
        }
    }

    return(result);
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

//2) Range with Step
console.log("\n2) Range with Step");
function rangeWithStep(startNum, finishNum, step) {
    var result = [];

    if(startNum <= finishNum){
        for(var i = startNum; i <= finishNum; i += step){
            result.push(i);
        }
    }
    else if(startNum > finishNum){
        for(var i = startNum; i >= finishNum; i -= step){
            result.push(i);
        }
    }

    return result;
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

//3) Sum of Range
console.log("\n3) Sum of Range");
function sum(startNum, finishNum=startNum, step = 1) {
    var arr_Range = rangeWithStep(startNum, finishNum, step);
    var sum = 0;
    for(var i = 0; i<arr_Range.length; i++){
        sum += arr_Range[i];
    }
    return sum;
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

//4) Array Multidimensi
console.log("\n4) Array Multidimensi");
function dataHandling(input){
    for(var y=0; y<input.length; y++){
        for(var x=0; x<input[y].length; x++){
            var string = "";
            switch(x){
                case 0: {string += "Nomor ID:   " + input[y][x]; break;}
                case 1: {string += "Nama Lengkap:   " + input[y][x]; break;}
                case 2: {string += "TTL: " + input[y][x] + " " + input[y][x+1]; break;}
                case 4: {string += "Hobi:   " + input[y][x] + "\n"; break;}
            }

            if(string != ""){
                console.log(string);
            }
        }
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

dataHandling(input);

//5) Balik Kata
console.log("\n5) Balik Kata");
function balikKata(string){
    var result = [];
    for(var i=string.length-1; i>=0; i--){
        result += string[i];
    }
    return result;
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

//6) Metode Array
console.log("\n6) Metode Array");
function dataHandling2(input){
    input[1] += "Elsharawy";
    input[2] = "Provinsi " + input[2];
    input.splice(input.length-1, 1, "Pria", "SMA Internasional Metro")
    console.log(input);

    date = input[3].split("/");
    switch(date[1]){
        case "01": {console.log("Januari") ;break;}
        case "02": {console.log("Februari") ;break;}
        case "03": {console.log("Maret") ;break;}
        case "04": {console.log("April") ;break;}
        case "05": {console.log("Mei") ;break;}
        case "06": {console.log("Juni") ;break;}
        case "07": {console.log("Juli") ;break;}
        case "08": {console.log("Agustus") ;break;}
        case "09": {console.log("September") ;break;}
        case "10": {console.log("Oktober") ;break;}
        case "11": {console.log("November") ;break;}
        case "12": {console.log("Desember") ;break;}
    }

    var yyddmm = [];
    yyddmm.push(date[2]);
    yyddmm.push(date[0]);
    yyddmm.push(date[1]);

    console.log(yyddmm);
    console.log(date.join("-"));
    console.log(input[1].substring(0, input[1].length-10));
}
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);