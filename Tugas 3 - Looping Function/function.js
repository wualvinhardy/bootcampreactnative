//1) Function Teriak()
console.log("1) Function Teriak");
function teriak(){
    return "Halo Sanbers!";
}
 
console.log(teriak()); // "Halo Sanbers!" 

//2) Function Kalikan()
console.log("\n2) Function Kalikan");
function kalikan(int1, int2){
    return int1*int2;
}

var num1 = 12;
var num2 = 4;
 
var hasilKali = kalikan(num1, num2);
console.log(hasilKali); // 48

//3) Function Introduce()
console.log("\n3) Function Introduce");
function introduce(name, age, address, hobby){
    string1 = "Nama saya " + name;
    string2 = ", umur saya " + age;
    string3 = " tahun, alamat saya di " + address;
    string4 = " dan saya punya hobby yaitu " + hobby + "!";
    result = string1 + string2 +string3 + string4;

    return result;
}

var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";
 
var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan); // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!"