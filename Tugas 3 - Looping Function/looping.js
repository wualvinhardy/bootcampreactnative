//1) Looping While
console.log("1) Looping While");
var number1 = 20;
var i = 0;
var increment = 2;

console.log("LOOPING PERTAMA");
while (i < 20){
    i += increment;
    console.log(i + " - I love coding");
}
console.log("LOOPING PERTAMA");
while(i > 0){
    console.log(i + " - I will become a mobile developer");
    i -= increment;
}

//2) Looping For
console.log("\n2) Looping For");
var number2 = 20;

for(var y=1; y <= number2; y++){
    process.stdout.write(y + " - ");
    if(y % 2 == 1){
        if(y % 3 == 0){
            console.log("I Love Coding");
        }
        else{
            console.log("Santai");
        }
    }
    else{
        console.log("Berkualitas");
    }
}

//3) Membuat Persegi Panjang
console.log("\n3) Membuat Persegi Panjang")
var length3 = 8, width3 = 4;
var box3 = "";

for(var y=0; y<width3; y++){
    for(var x=0; x<length3; x++){
       box3 += "#"; 
    }
    box3 += "\n";
}
console.log(box3);

//4) Membuat Tangga
console.log("\n4) Membuat Tangga");
var base4 = 7;
var triangle4 = "";
for(var y=0; y<base4; y++){
    for(var x=0; x<=y; x++){
        triangle4 += "#";
    }
    triangle4 += "\n";
}
console.log(triangle4);

//5) Membuat Papan Catur
console.log("\n5) Membuat Papan Catur");
var side5 = 8;
var board = "";
for  (var y=0; y < side5; y++){
    for (var x=0; x < side5; x++){
        if(x%2 == 0 && y%2 == 0){
            board += " ";
        }
        else if(x%2 == 1 && y%2 == 1){
            board += " ";
        }
        else{
            board += "#";
        }
    }
    board += "\n";
}
console.log(board);